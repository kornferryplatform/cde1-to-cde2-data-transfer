const jsonfile = require('jsonfile');
const cde1Api = require('./api/cde1Api');
const nodeUtils = require('./node');
const prop = require('./prop');
const props = require('./props');
const props2d = require('./props2d');

const next = function(entity, entities) {
    return new Promise(resolve => {
        if (!entities[entity.cguid]) {
            switch (entity.type) {
            case 'prop':
                entities = prop.process(entity, entities);
                resolve(entities);
                break;
            case 'props':
                entities = props.process(entity, entities);
                resolve(entities);
                break;
            case 'props2d':
                entities = props2d.process(entity, entities);
                resolve(entities);
                break;
            case 'category':
                processCategory(entity, entities).then(returnEntities => {
                    resolve(returnEntities);
                });
                break;
            case undefined:
                processManifest(entity, entities).then(returnEntities => {
                    resolve(returnEntities);
                });
                break;
            default:
                resolve(entities);
                break;
            }
        }
    });
};

const process = function(entity, entities) {
    return new Promise((resolve, reject) => {
        if (!entities[entity.cguid]) {
            if (entity.extends && entity.extends.cguid) {
                cde1Api
                    .retrieve(entity.type, entity.extends.cguid)
                    .then(baseEntity => {
                        jsonfile.writeFileSync(
                            'data/source/' +
                                entity.type +
                                '/' +
                                baseEntity.cguid +
                                '.json',
                            baseEntity,
                            { spaces: 2 }
                        );
                        Promise.all([
                            next(baseEntity, entities),
                            next(entity, entities)
                        ]).then(() => {
                            resolve(entities);
                        });
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                next(entity, entities).then(() => {
                    resolve(entities);
                });
            }
        }
    });
};

const processManifest = function(manifest, entities) {
    return new Promise(resolve => {
        const manifestObj = {};
        manifestObj.id = manifest.id;
        manifestObj.type = 'manifest';
        manifestObj.value = [];

        const childPromises = [];
        for (let i = 0, len = manifest.value.length; i < len; i++) {
            const nodeSrc = manifest.value[i];
            manifestObj.value.push(
                nodeUtils.create(
                    nodeSrc.key,
                    nodeSrc.cguid,
                    nodeSrc.type,
                    nodeSrc.metaData
                )
            );
            childPromises.push(process(nodeSrc, entities));
        }
        Promise.all(childPromises).then(() => {
            entities[manifestObj.id] = manifestObj;
            console.log(
                '\t  Adding ' + manifestObj.type + ': ' + manifestObj.id
            );
            resolve(entities);
        });
    });
};

const processCategory = function(category, entities) {
    return new Promise(resolve => {
        const isExtension = category.hasOwnProperty('extends');
        const categoryObj = {};
        categoryObj.cguid = category.cguid;
        categoryObj.type = 'category';
        if (category.tags) {
            categoryObj.tags = category.tags;
        }
        if (isExtension) {
            categoryObj.extends = {
                cguid: category.extends.cguid
            };
        }
        categoryObj.value = [];

        const childPromises = [];
        for (let i = 0, len = category.value.length; i < len; i++) {
            const nodeSrc = category.value[i];
            if (!isExtension || nodeSrc.hasOwnProperty('extension')) {
                categoryObj.value.push(
                    nodeUtils.create(
                        nodeSrc.key,
                        nodeSrc.cguid,
                        nodeSrc.type,
                        nodeSrc.metaData,
                        nodeSrc.extension
                    )
                );
                childPromises.push(process(nodeSrc, entities));
            } else if (
                isExtension &&
                (nodeSrc.type === 'exclude' || nodeSrc.type === 'include')
            ) {
                categoryObj.value.push(nodeSrc);
            }
        }

        Promise.all(childPromises).then(() => {
            entities[categoryObj.cguid] = categoryObj;
            console.log(
                '\t  Adding ' + categoryObj.type + ': ' + categoryObj.cguid
            );
            resolve(entities);
        });
    });
};

module.exports = {
    process: process,
    processCategory: processCategory,
    processManifest: processManifest
};
