const request = require('superagent');

const uri = 'http://52.205.128.33:8045/dev/contentDictionary/state/manifests';
const authorization = 'Basic YWRtaW46TUxQYXNzd29yZDEyMyQ=';

const upload = function(payload) {
    console.log('\tSaving: ' + payload.payload.scope);
    return new Promise((resolve, reject) => {
        request
            .put(uri)
            .set('Authorization', authorization)
            .set('Content-Type', 'application/json')
            .send(payload)
            .end((err, res) => {
                if (err === null && res.body.success !== false) {
                    resolve(res.body);
                } else {
                    if (res.body) {
                        reject(res.body);
                    } else {
                        reject(res.status);
                    }
                }
            });
    });
};

module.exports = {
    upload: upload
};
