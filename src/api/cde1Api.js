const request = require('superagent');

const uri = 'http://52.86.204.94:8045/v1/contentDictionary/';
const retrieveQuery = {
    format: 'jsonauthoring',
    includeAllLanguages: true
};
const authorization = 'Basic YWRtaW46TUxQYXNzd29yZDEyMyQ=';

const retrieve = function(type, id) {
    console.log('\tRetrieving ' + type + ': ' + id);
    return new Promise((resolve, reject) => {
        request
            .get(uri + type + '/' + id)
            .set('Authorization', authorization)
            .query(retrieveQuery)
            .end((err, res) => {
                if (err === null && res.body.success !== false) {
                    resolve(res.body);
                } else {
                    reject(res.status);
                }
            });
    });
};

module.exports = {
    retrieve: retrieve
};
