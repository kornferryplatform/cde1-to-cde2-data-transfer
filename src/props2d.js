const props = require('./props');

const process = function(props2d, entities) {
    const props2dObj = create(props2d);

    entities[props2dObj.cguid] = props2dObj;
    console.log('\t  Adding ' + props2dObj.type + ': ' + props2dObj.cguid);
    return entities;
};

const create = function(props2d) {
    const isExtension = props2d.hasOwnProperty('extends');
    const props2dObj = {};
    props2dObj.cguid = props2d.cguid;
    props2dObj.type = 'props2d';
    if (props2d.tags) {
        props2dObj.tags = props2d.tags;
    }
    if (isExtension) {
        props2dObj.extends = {
            cguid: props2d.extends.cguid
        };
    }
    props2dObj.value = [];

    for (let i = 0, len = props2d.value.length; i < len; i++) {
        const propsSrc = props2d.value[i];
        if (!isExtension || propsSrc.hasOwnProperty('extension')) {
            const propsObj = props.create(propsSrc);
            propsObj.key = propsSrc.key;
            propsObj.extension = propsSrc.extension;
            props2dObj.value.push(propsObj);
        } else if (
            isExtension &&
            (propsSrc.type === 'exclude' || propsSrc.type === 'include')
        ) {
            props2dObj.value.push(propsSrc);
        }
    }

    return props2dObj;
};

module.exports = {
    create: create,
    process: process
};
