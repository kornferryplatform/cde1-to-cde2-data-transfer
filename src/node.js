const create = function(key, cguid, type, metaData, extension) {
    const node = {
        key,
        cguid,
        type
    };

    if (metaData) {
        node.metaData = metaData;
    }

    if (extension) {
        node.extension = extension;
    }

    return node;
};

module.exports = {
    create: create
};
