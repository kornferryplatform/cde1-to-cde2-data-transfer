const process = function(prop, entities) {
    const propObj = create(prop);

    entities[propObj.cguid] = propObj;
    console.log('\t  Adding ' + propObj.type + ': ' + propObj.cguid);
    return entities;
};

const create = function(prop) {
    const propObj = {};
    propObj.cguid = prop.cguid;
    propObj.type = 'prop';
    if (prop.metaData) {
        propObj.metaData = true;
    }
    if (prop.tags) {
        propObj.tags = prop.tags;
    }

    propObj.value = {};

    propObj.value['en-US'] = prop.value;

    if (prop.langs) {
        for (let i = 0, len = prop.langs.length; i < len; i++) {
            const lang = prop.langs[i];
            propObj.value[lang.lang] = lang.value;
        }
    }

    return propObj;
};

module.exports = {
    create: create,
    process: process
};
