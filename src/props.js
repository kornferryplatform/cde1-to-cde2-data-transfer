const prop = require('./prop');

const process = function(props, entities) {
    const propsObj = create(props);

    entities[propsObj.cguid] = propsObj;
    console.log('\t  Adding ' + propsObj.type + ': ' + propsObj.cguid);
    return entities;
};

const create = function(props) {
    const isExtension = props.hasOwnProperty('extends');
    const propsObj = {};
    if (props.cguid) {
        propsObj.cguid = props.cguid;
    }
    propsObj.type = 'props';
    if (props.tags) {
        propsObj.tags = props.tags;
    }
    if (isExtension) {
        propsObj.extends = {
            cguid: props.extends.cguid
        };
    }
    propsObj.value = [];

    for (let i = 0, len = props.value.length; i < len; i++) {
        const propSrc = props.value[i];
        if (!isExtension || propSrc.hasOwnProperty('extension')) {
            const propObj = prop.create(propSrc);
            propObj.key = propSrc.key;
            propObj.extension = propSrc.extension;
            propsObj.value.push(propObj);
        } else if (
            isExtension &&
            (propSrc.type === 'exclude' || propSrc.type === 'include')
        ) {
            propsObj.value.push(propSrc);
        }
    }

    return propsObj;
};

module.exports = {
    create: create,
    process: process
};
