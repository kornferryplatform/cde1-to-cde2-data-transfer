const jsonfile = require('jsonfile');
const toArray = require('lodash/toArray');
const cde1Api = require('./api/cde1Api');
const cde2Api = require('./api/cde2Api');
const containersJS = require('./containers');

console.log('STARTING...');
const manifestId = process.argv[2];
const shouldUpload = process.argv[3] === 'upload';

if (manifestId) {
    cde1Api.retrieve('manifest', manifestId).then(manifest => {
        jsonfile.writeFileSync(
            'data/source/manifest/' + manifestId + '.json',
            manifest,
            { spaces: 2 }
        );
        containersJS.process(manifest, {}).then(entities => {
            const payload = {
                payload: {
                    scope: manifestId,
                    objects: toArray(entities)
                }
            };
            jsonfile.writeFileSync(
                'data/results/' + manifestId + '.json',
                payload,
                { spaces: 2 }
            );
            if (shouldUpload) {
                cde2Api
                    .upload(payload)
                    .then(saveResult => {
                        console.log('Results: ');
                        console.log(JSON.stringify(saveResult, null, 2));
                        console.log('...FINISHED');
                    })
                    .catch(error => {
                        console.log('ERROR');
                        console.log(JSON.stringify(error, null, 2));
                    });
            } else {
                console.log('...FINISHED');
            }
        });
    });
} else {
    console.log('MANIFEST ID IS REQUIRED');
}
